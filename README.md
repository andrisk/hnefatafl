# Hnefatafl

## Description

Implementation of an ancient Viking game - specifically Irish variant called Brandubh in Rust programming language. The game was implemented according to a description on a [www.ancientgames.org](https://www.ancientgames.org/hnefatafl-brandubh) website where you can read the rules.

The project was started as an excercise in Rust programming with a hope that some people will find it useful.

Currently there is a text terminal version or gui version for Ubuntu Touch smartphone OS.

## Installation
Currently there is only a package for ubuntu touch available.

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/hnefatafl.andrisk)

## Build
After cloning the repository and installing rust development tools according to an instructions in [The Rust Book](https://doc.rust-lang.org/stable/book/ch01-01-installation.html) you should be able to build (or run) the terminal version of the software by one of the following commands from hnefatafl_text folder:

`cargo build`

`cargo run`

For building the mobile qml version for ubuntu touch, you additionaly need  [clickable tool](https://clickable-ut.dev/en/latest/install.html) and also qt development tools (usually from linux package manager, or qt website if you are using windows).

Then it should be possible to try the app in a docker container on desktop by executing following command in hnefatafl_qml folder:

`clickable desktop`

or for launching the application on a ubuntu touch smarphone connect the phone with enabled developer mode to pc by usb cable:

`clickable`

## Usage
Specify your turn by a 4 character text and press enter. Example: B4B3 for moving piece from B4 to B3.

## Support
It is possible to raise an issue [here](https://gitlab.com/andrisk/hnefatafl/-/issues) on gitlab.

## Roadmap

version 0.1:

 - [x] game engine with rules
 - [x] basic AI player
 - [x] terminal interface
 - [x] basic qml interface

version 0.2:
 - [x] move animations

future versions:
 - [ ] AI player improvements (better heuristics, multithreaded, ..)
 - [ ] Network multiplayer

## Contributing

I am open for contributions from anyone who would like to improve this project. By contributing you agree that your changes will be distributed under the same license as this project (GPLv3).

## License
Copyright (C) 2021-2023 Andrej Trnkóci

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](http://www.gnu.org/licenses/)
