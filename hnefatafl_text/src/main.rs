/*
 * Copyright (C) 2021-2022  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::io;
use tafl_engine::Board;
use tafl_engine::Players;

fn main() {
    let mut board = Board::new();
    board.set_up();

    loop {
        board.print();
        if board.current_player == Players::Attackers {
            //println!("Attackers turn");
            //let (_score, chosen_move) = board.clone().alpha_beta_max(-10001, 10001, 4);
            //println!("Minmax chosen move: {}", chosen_move);
            //board.make_move_str_coord(chosen_move);
            board.make_ai_move(4);
        } else {
            println!("Defenders turn");
            let mut move_str = String::new();
            io::stdin()
                .read_line(&mut move_str)
                .expect("Failed to read line");
            println!("your move: {}", move_str);
            if !board.make_move_str_coord(move_str) {
                println!("Illegal move!");
         
            }
            //board.make_ai_move(4);
        }

        match board.winner {
            Some(ref player) => {
                if *player == Players::Attackers {
                    board.print();
                    println!("Attackers won");
                    break;
                } else if *player == Players::Defenders {
                    board.print();
                    println!("Defenders won");
                    break;
                }
            }
            None => {}
        }
    }
}

