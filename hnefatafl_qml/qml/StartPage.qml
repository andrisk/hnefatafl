// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2021 - 2022 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: startPageRoot
    signal startNewGame(var attackerSetting, var defenderSetting)

    ColumnLayout {
        spacing: 2
        width: parent.width
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        id: columnItem
        anchors.margins: startPageRoot.width / 20

        ComboBox {
            id: playersSelectionCb
            Layout.fillWidth: true
            model: [
                i18n.tr("Human player as attackers vs. AI"),
                i18n.tr("Human player as defenders vs. AI"),
                i18n.tr("Two human players")
            ]

    	    popup: Popup {
                width: playersSelectionCb.width
    		    implicitHeight: contentHeight
		        padding: 0
		        margins: 0
		        spacing: 0
		        font.pixelSize: playersSelectionCb.height * 0.35
    	    	contentItem: ListView {
    	    	    clip: true
    	    	    spacing: 0
    	    	    implicitHeight: contentHeight
    	    	    model: playersSelectionCb.popup.visible ? playersSelectionCb.delegateModel : null
	        	    currentIndex: playersSelectionCb.highlightedIndex
        		}
            }
        }

        Button {
            id: newGameButton
            Layout.alignment: Qt.AlignCenter
            Layout.topMargin: startPageRoot.width / 20
            Layout.bottomMargin: startPageRoot.width / 20
            text: i18n.tr('New Game')
            onClicked: {
                if (playersSelectionCb.currentIndex === 0) {
                    startPageRoot.startNewGame("Human", "AI")
                    stack.push(gamePage)
                }
                else if (playersSelectionCb.currentIndex === 1) {
                    startPageRoot.startNewGame("AI", "Human")
                    stack.push(gamePage)
                }
                else if (playersSelectionCb.currentIndex === 2) {
                    startPageRoot.startNewGame("Human", "Human")
                    stack.push(gamePage)
                }
            }
        }

        Text {
            Layout.fillWidth: true
            Layout.fillHeight: true
            wrapMode: Text.Wrap
            verticalAlignment: Text.AlignBottom
            text: "Game rules can be found on a site of the World Tafl Federation (<a href=\"https://aagenielsen.dk/brandubh2_rules_en.pdf\">Brandubh rules (PDF)</a>). \
                    Not all rules as defined in the document are implemented in the current version of this application. "
            onLinkActivated: (link) => Qt.openUrlExternally(link)
        }
    }
}
