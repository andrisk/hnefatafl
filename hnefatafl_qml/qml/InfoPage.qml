// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2022, 2024  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQml.Models 2.1

Page
{
    id: appInfoPage
    ObjectModel
    {
        id: appInfoModel

        BasicAppInfo
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 3
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("Brandubh rules (PDF)")
            targetUrl: "https://aagenielsen.dk/brandubh2_rules_en.pdf"
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("Source code")
            targetUrl: "https://gitlab.com/andrisk/hnefatafl"
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("Engine source code")
            targetUrl: "https://gitlab.com/andrisk/tafl-engine"
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("Report issue")
            targetUrl: "https://gitlab.com/andrisk/hnefatafl/issues"
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("License") + ": GPLv3"
            targetUrl: "https://www.gnu.org/licenses/gpl-3.0.html"
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("Author")+ ": Andrej Trnkóci"
            targetUrl: "https://gitlab.com/andrisk/"
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("Translation") + " (fr): Anne Onyme"
            targetUrl: "https://gitlab.com/Anne17"
        }

        LinkReferenceItem
        {
            width: appInfoPage.width - getMarginSpaceForItemSize(appInfoPage.width, appInfoPage.height)
            height: Math.max(appInfoPage.width, appInfoPage.height) / 20
            labelText: i18n.tr("Translation") + " (nl): Heimen Stoffels"
            targetUrl: "https://gitlab.com/Vistaus"
        }
    }

    ListView
    {
        id: appInfoListView
        model: appInfoModel
        spacing: 3
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width - getMarginSpaceForItemSize(parent.width, parent.height)
        height: parent.height - getMarginSpaceForItemSize(parent.width, parent.height)
    }

    function getMarginSpaceForItemSize(width, height)
    {
        return Math.min(width, height) /10
    }
}
