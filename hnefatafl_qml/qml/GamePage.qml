// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2021 - 2023 Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import QtQuick.Dialogs 1.3
import QmlTaflEngineInterface 1.0

Page
{
    id: gamePageRoot
    signal boardChangedSignal()
    signal unselectPieceSignal()
    signal startNewGame(var attackerSetting, var defenderSetting)
    signal startMoveAnimationSignal(var startTileX, var startTileY, var destTileX, var destTileY)
    signal startRemoveAnimationSignal(var targetTileX, var targetTileY)
    signal resetAnimationsSignal()
    signal moveAnimationFinishedSignal()
    signal makeAiTurnSignal()

    property string attackersPlayerSetting : ""
    property string defendersPlayerSetting : ""

    MessageDialog {
        id: endGameDialog
        title: "Game ended!"
        text: "Somebody just won!"
        onAccepted: {
            stack.pop()
        }
    }

    Popup {
        id: endGamePopup
        modal: false

        x: gamePageRoot.height > gamePageRoot.width ? gamePageRoot.width * 0.25 : gameBoard.x + gameBoard.width
        y: gamePageRoot.height > gamePageRoot.width ? gameBoard.y + gameBoard.height : gamePageRoot.height * 0.25
        width: gamePageRoot.width < gamePageRoot.height ? gamePageRoot.width / 2 : gamePageRoot.height / 2
        height: gamePageRoot.width < gamePageRoot.height ? gamePageRoot.width / 2 : gamePageRoot.height / 2
        dim: true

        onClosed: {
            stack.pop()
        }
        ColumnLayout {
            anchors.fill: parent
            Text {
                id: endGamePopupText
                text: ""
                color: theme.palette.normal.foregroundText
                wrapMode: Text.WordWrap
                Layout.leftMargin: parent.height / 10
                Layout.rightMargin: parent.height / 10
                Layout.topMargin: parent.height / 10
                Layout.fillWidth: true
            }
            Button {
                text: "OK"
                Layout.margins: parent.height / 10
                Layout.fillWidth: true
                onClicked: {
                    endGamePopup.close()
                    stack.pop()
                }
            }
        }
    }

    QmlTaflEngineInterface {
        id: taflEngine
    }

    Grid
    {
        columns: gamePageRoot.availableWidth < gamePageRoot.availableHeight ? 1 : 2
        rows: gamePageRoot.availableWidth < gamePageRoot.availableHeight ? 2 : 1
        padding:  parent.width < parent.height ? parent.width * 0.05 : parent.height * 0.05
        spacing: parent.width < parent.height ? parent.width * 0.05 : parent.height * 0.05
        anchors.fill: parent
        horizontalItemAlignment: Grid.AlignHCenter
        verticalItemAlignment: Grid.AlignVCenter

        Item {
            id: boardItem
            width: parent.width < parent.height ? parent.width * 0.9 : parent.height * 0.9
            height: parent.width < parent.height ? parent.width * 0.9 : parent.height * 0.9
            property int boardRows : 7
            property int boardColumns : 7

            states: [
                State {
                    name: "player_turn"
                },
                State {
                    name: "ai_turn"
                },
                State {
                    name: "animating"
                }
            ]

            GridLayout {
                id: gameBoard
                width: parent.width;
                height: parent.height

                property int selectedX: -1
                property int selectedY: -1

                columns: 7
                Repeater {
                    id: tilesRepeater
                    model: 49
                    Rectangle { // tile rectangle
                        id: tileRectangle

                        property int tileX: indexToXcoord(index)
                        property int tileY: indexToYcoord(index)
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.margins: 0
                        color: "gray"

                        function paintTile() {
                            if (taflEngine.is_corner(tileX, tileY)) {
                                color = "red";
                            } else if (taflEngine.is_throne(tileX, tileY)) {
                                color = "yellow";
                            } else {
                                color = "gray";
                            }
                        }

                        Component.onCompleted: {
                            paintTile()
                        }

                    }
                }

                Component.onCompleted: {
                    boardChangedSignal.connect(afterUpdateChecks)
                    makeAiTurnSignal.connect(make_ai_turn)
                }
            }

            GridLayout
            {
                id: piecesGrid
                width: parent.width;
                height: parent.height
                columns: 7

                Repeater {
                    id: pieceRepeater
                    model: 49

                    Rectangle {
                        id: pieceRectangle

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        color: "transparent"

                        property int tileX: indexToXcoord(index)
                        property int tileY: indexToYcoord(index)

                        x: 0
                        y: 0
                        opacity: 1
                        property string text: ""
                        property color textColor: "black"

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if (boardItem.state == "player_turn") {
                                    if (taflEngine.is_tile_occupied_by_current_player(parent.tileX, parent.tileY)) {

                                        gamePageRoot.unselectPieceSignal();
                                        pieceRepeater.itemAt(index).textColor = "orange";
                                        gameBoard.selectedX = parent.tileX
                                        gameBoard.selectedY = parent.tileY
                                    }
                                    else if (gameBoard.selectedX > -1 && gameBoard.selectedY > -1) {
                                        var move = tileNumCoordsToStringCoords(gameBoard.selectedX, gameBoard.selectedY)
                                                 + tileNumCoordsToStringCoords(parent.tileX, parent.tileY);
                                        if (taflEngine.make_move(move)) {
                                            var startX = gameBoard.selectedX
                                            var startY = gameBoard.selectedY

                                            gameBoard.selectedX = -1
                                            gameBoard.selectedY = -1
                                            gamePageRoot.unselectPieceSignal()
                                            boardItem.state = "animating"
                                            startMoveAnimationSignal(startX, startY, parent.tileX, parent.tileY)
                                        }
                                    }
                                }
                            }
                        }

                        function updatePiece() {
                            var pieceStr = taflEngine.get_tile_content(tileX, tileY);
                            if (pieceStr.localeCompare("King") === 0) {
                                text = "\u{2654}";
                            } else if (pieceStr.localeCompare("Defender") === 0) {
                                text = "\u{2659}";
                            } else if (pieceStr.localeCompare("Attacker") === 0) {
                                text = "\u{265F}";
                            } else if (pieceStr.localeCompare("None") === 0) {
                                text = "";
                            }
                        }

                        function unselect() {
                            textColor = "black";
                            gameBoard.selectedX = -1;
                            gameBoard.selectedY = -1;
                        }


                        Text {
                            id: pieceCharacter
                            text: pieceRectangle.text
                            font.pixelSize: parent.width
                            color: pieceRectangle.textColor
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter

                        }
                        NumberAnimation {
                            from: 0
                            to: -50
                            duration: 1000
                            target: pieceRectangle
                            properties: "y"
                            loops: 1 //Animation.Infinite
                            running: false //tileRectangle.tileX === 2 && tileRectangle.tileY === 3

                            property real startCoordX: 0
                            property real startCoordY: 0
                            property bool animationAtEnd: false
                            onFinished:
                            {
                                animationAtEnd = true
                                moveAnimationFinishedSignal()
                            }


                            function startMoveAnimation(startX, startY, destX, destY) {
                                if ( startX === tileX && startY === tileY) {
                                    //print("index: " + index)
                                    var startRectangle = getItemByCoords(startX, startY)
                                    var destRectangle = getItemByCoords(destX, destY)

                                    startCoordX = startRectangle.x
                                    startCoordY = startRectangle.y

                                    //startRectangle.color = "yellow"
                                    //destRectangle.color = "purple"

                                    if (startX !== destX) {
                                        properties = "x"
                                        from = startRectangle.x
                                        to = destRectangle.x
                                    }
                                    else {
                                        properties = "y"
                                        from = startRectangle.y
                                        to =  destRectangle.y
                                    }
                                    running = true
                                }
                            }

                            function resetAnimation() {
                                if (animationAtEnd)
                                {
                                    stop()
                                    pieceRectangle.x = startCoordX
                                    pieceRectangle.y = startCoordY
                                    //print("reset to x: " + startCoordX + " y: " + startCoordY)
                                }
                            }

                            Component.onCompleted: {
                                //connect functions to a signals
                                startMoveAnimationSignal.connect(startMoveAnimation)
                                resetAnimationsSignal.connect(resetAnimation)
                            }
                        }

                        Component.onCompleted: {
                            boardChangedSignal.connect(updatePiece)
                            unselectPieceSignal.connect(unselect)
                            moveAnimationFinishedSignal.connect(moveAnimationFinishedHandler)
                        }
                        /*
                        NumberAnimation on opacity {
                            from: 1
                            to: 0
                            duration: 1000
                            loops: 1
                            running: false

                            function startRemoveAnimation(targetTileX, targetTileY) {
                                if ( targetTileX === tileX && targetTileY === tileY) {
                                    running = true
                                }
                            }

                            function resetAnimation() {

                            }

                            Component.onCompleted: {
                                //connect functions to a slot
                                startRemoveAnimationSignal.connect(startRemoveAnimation)
                                resetAnimationsSignal.connect(resetAnimation)


                            }
                        }*/
                    }
                }
            }
        }

        Button {
            text: i18n.tr('Exit game')
            onClicked: {
                stack.pop()
            }
        }
    }
    Component.onCompleted: {
        gamePageRoot.startNewGame.connect(startNewGameHandler)
    }

    Timer {
        id: aiTurnTimer
        interval: 0; running: false; repeat: false
        onTriggered: make_ai_turn()
    }

    function stringCoordsToTileNumCoords(tileStr)
    {
        if (tileStr.length > 1)
        {
            return ["ABCDEFGHIJKLM".indexOf(tileStr[0]), parseInt(tileStr[1], 10) - 1]
        }
        else
        {
            return [0, 0];
        }
    }

    function tileNumCoordsToStringCoords(tileX, tileY) {
        return "ABCDEFGHIJKLM"[tileX] + (tileY + 1).toString();
    }

    function is_ai_turn() {
        return ((taflEngine.current_player() === "Attackers" && gamePageRoot.attackersPlayerSetting === "AI") ||
                (taflEngine.current_player() === "Defenders" && gamePageRoot.defendersPlayerSetting === "AI"))
    }

    function is_player_turn() {
        return !is_ai_turn()
    }

    function getItemByCoords(tileX, tileY) {
        var index = tileX + boardItem.boardColumns * (boardItem.boardColumns - 1 - tileY)
        return tilesRepeater.itemAt(index)
    }

    function startNewGameHandler(attackerSetting, defenderSetting ) {
        gamePageRoot.attackersPlayerSetting = attackerSetting
        gamePageRoot.defendersPlayerSetting = defenderSetting
        taflEngine.set_up_board()
        boardItem.state = is_ai_turn() ? "ai_turn" : "player_turn"
        gamePageRoot.boardChangedSignal()
        gamePageRoot.unselectPieceSignal()
        moveAnimationFinishedSignal()
    }

    function moveAnimationFinishedHandler() {
        resetAnimationsSignal()
        gamePageRoot.boardChangedSignal()
        if (is_ai_turn()) {
            boardItem.state = "ai_turn"
            aiTurnTimer.start()
        }
        else {
            boardItem.state = "player_turn"
        }
    }

    function afterUpdateChecks() {
        var winner = taflEngine.check_winner();
        if (winner === "Attackers") {
            endGamePopupText.text = "Attackers won!"
            endGamePopup.open()
        } else if (winner === "Defenders") {
            endGamePopupText.text = "Defenders won!"
            endGamePopup.open()
        }
    }

    function make_ai_turn() {
        if (is_ai_turn() && (taflEngine.check_winner() === "None")) {
            var aiMoveStr = taflEngine.make_ai_move(4)
            const [fromX, fromY] = stringCoordsToTileNumCoords(aiMoveStr.substring(0,2))
            const [toX, toY] = stringCoordsToTileNumCoords(aiMoveStr.substring(2,4))
            startMoveAnimationSignal(fromX, fromY, toX, toY)
        }
    }

    function indexToXcoord(index)
    {
        return index % boardItem.boardColumns
    }

    function indexToYcoord(index)
    {
        return (boardItem.boardRows - 1) - Math.trunc(index / boardItem.boardRows)
    }
}
