// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2020 - 2021, 2023 - 2024  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as UITK

Column
{
    UITK.LomiriShape {
        width: Math.min(parent.width, parent.height) * 0.70
        height: width
        radius: "large"
        anchors.horizontalCenter: parent.horizontalCenter
        source: Image {
            mipmap: true
            source: "qrc:/assets/logo.svg"
        }
    }

    Label
    {
        anchors.horizontalCenter: parent.horizontalCenter
        text: i18n.tr('Hnefatafl')
        height: Math.min(parent.width, parent.height) * 0.20

        font.pixelSize: height * 0.8
    }
    Label
    {
        anchors.horizontalCenter: parent.horizontalCenter
        text: i18n.tr("Version") + " 0.2.2" //+ Qt.application.version
        height: Math.min(parent.width, parent.height) * 0.10
        font.pixelSize: height * 0.8
    }
}
