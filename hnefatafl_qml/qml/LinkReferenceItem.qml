// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2020 - 2021, 2023 - 2024  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * FretboardTrainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as UITK

Rectangle
{
    color: theme.palette.normal.foreground
    radius: height / 3
    border.color: theme.palette.normal.foregroundText
    border.width: 1

    property string labelText;
    property string targetUrl;

    Label
    {
        text: labelText
        anchors.left: parent.left
        anchors.right: nextSymbol.left
        height: parent.height
        padding: parent.height / 5
        fontSizeMode: Text.Fit
        font.pixelSize: height
        MouseArea
        {
            anchors.fill: parent
            onClicked: Qt.openUrlExternally(targetUrl)
        }
    }
    UITK.Icon
    {
        id: nextSymbol
        name: "next"
        width: parent.height * 0.8
        height: parent.height * 0.8
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        color: theme.palette.normal.foregroundText
        MouseArea
        {
            anchors.fill: parent
            onClicked: Qt.openUrlExternally(targetUrl)
        }
    }
}
