// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2021 - 2022  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import Lomiri.Components 1.3 as UITK
import QtQuick.Layouts 1.12
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.3

ApplicationWindow {
    id: root
    objectName: 'mainView'

    width: units.gu(45)
    height: units.gu(75)
    visible: true

    StackView
    {
        id: stack
        initialItem: startPage
        anchors.fill: parent
        width: parent.width
        StartPage {
            id: startPage
            width: parent.width
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            header: UITK.PageHeader {
                title: i18n.tr('Hnefatafl')
                trailingActionBar.actions: [
                    UITK.Action
                    {
                        iconName: "info"
                        text: i18n.tr("Info")
                        onTriggered: stack.push(infoPage)
                    }
                ]
            }
        }

        GamePage {
            id: gamePage
            width: parent.width
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            visible: false
        }

        InfoPage {
            id: infoPage
            width: parent.width
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            visible: false
            header: UITK.PageHeader {
                title: i18n.tr('Hnefatafl')
                leadingActionBar.actions: [
                    UITK.Action
                    {
                        iconName: "back"
                        text: i18n.tr("back")
                        onTriggered: stack.pop()
                    }
                ]
            }
        }

        Component.onCompleted: {
            startPage.startNewGame.connect(gamePage.startNewGame)
        }
    }
}
