qrc!(qml_resources,
    "/" {
        "qml/Main.qml",
        "qml/StartPage.qml",
        "qml/GamePage.qml",
        "qml/InfoPage.qml",
        "qml/BasicAppInfo.qml",
        "qml/LinkReferenceItem.qml"
    },
);

qrc!(assets_resources,
    "/" {
        "assets/logo.svg"
    },
);

pub fn load() {
    qml_resources();
    assets_resources();
}
