/*
 * Copyright (C) 2021-2023  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hnefatafl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate cstr;
#[macro_use]
extern crate qmetaobject;

use std::env;
use std::path::PathBuf;

use gettextrs::{bindtextdomain, textdomain};
use qmetaobject::*;
use tafl_engine::Board;
use tafl_engine::PieceType;
use tafl_engine::Players;

mod qrc;

#[derive(QObject,Default)]
struct Greeter {
    base : qt_base_class!(trait QObject),
    name : qt_property!(QString; NOTIFY name_changed),
    name_changed : qt_signal!(),
    compute_greetings : qt_method!(fn compute_greetings(&self, verb : String) -> QString {
        return (verb + " " + &self.name.to_string()).into()
    })
}

#[derive(QObject,Default)]
struct QmlTaflEngineInterface {
    base : qt_base_class!(trait QObject),
    board : Board,
    get_tile_content : qt_method!(fn get_tile_content(&self, x: usize, y: usize) -> QString {
            let piece = self.board.get_tile_content(x,y);
            match piece {
                Some(PieceType::King) => { return "King".into(); }
                Some(PieceType::Attacker) => { return "Attacker".into(); }
                Some(PieceType::Defender) => { return "Defender".into(); }
                _ => { return "None".into(); }
            }
            //"King".into()

    }),

    set_up_board: qt_method!(fn set_up_board(&mut self) {
        self.board.set_up();
    }),

    is_corner : qt_method!(fn is_corner(&self, x: usize, y: usize) -> bool {
        Board::is_corner(x,y)
    }),

    is_throne : qt_method!(fn is_throne(&self, x: usize, y: usize) -> bool {
        Board::is_throne(x,y)
    }),

    is_tile_occupied_by_current_player : qt_method!(fn is_tile_occupied_by_current_player(&self, x: usize, y: usize) -> bool {
        self.board.is_tile_occupied_by_current_player(x,y)
    }),

    make_ai_move : qt_method! (fn make_ai_move(&mut self, depth: usize) -> QString {
    	let move_option = self.board.make_ai_move(depth);
		match move_option {
        	Some(move_str) => { return move_str.into(); }
        	None => { return String::new().into(); }
        }
    }),

    make_move : qt_method! (fn make_move(&mut self, move_str: String) -> bool {
        return self.board.make_move_str_coord(move_str);
    }),

    check_winner : qt_method! (fn check_winner(&self) -> QString {
        match self.board.winner {
            Some(Players::Attackers) => { return "Attackers".into(); }
            Some(Players::Defenders) => { return "Defenders".into(); }
            None => { return "None".into(); }
        }
    }),

    current_player : qt_method! (fn current_player(&self) -> QString {
        match self.board.current_player {
            Players::Attackers => { return "Attackers".into(); }
            Players::Defenders => { return "Defenders".into(); }
        }
    }),

}
fn main() {
    init_gettext();
    unsafe {
        cpp! { {
            #include <QtCore/QCoreApplication>
            #include <QtCore/QString>
        }}
        cpp!{[]{
            QCoreApplication::setApplicationName(QStringLiteral("hnefatafl.andrisk"));
        }}
    }
    QQuickStyle::set_style("Suru");
    qrc::load();
    qml_register_type::<Greeter>(cstr!("Greeter"), 1, 0, cstr!("Greeter"));
    qml_register_type::<QmlTaflEngineInterface>(cstr!("QmlTaflEngineInterface"), 1, 0, cstr!("QmlTaflEngineInterface"));
    let mut engine = QmlEngine::new();
    engine.load_file("qrc:/qml/Main.qml".into());
    engine.exec();
}


fn init_gettext() {
    let domain = "hnefatafl.andrisk";
    textdomain(domain).expect("Failed switching the textdomain");

    let app_dir = env::var("APP_DIR").expect("Failed to read the APP_DIR environment variable");

    let mut app_dir_path = PathBuf::from(app_dir);
    if !app_dir_path.is_absolute() {
        app_dir_path = PathBuf::from("/usr");
    }

    let path = app_dir_path.join("share/locale");

    bindtextdomain(domain, path.to_str().unwrap()).expect("Failed to bind to a textdomain");
}
